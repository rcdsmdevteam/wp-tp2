﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using System.Windows.Data ;
using Tp2.Resources;

namespace Tp2
{
    public class SexeConverter : IValueConverter
    {

        object IValueConverter.Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((bool)value) return "Assets\\images\\garcon.jpg";
            else return "Assets\\images\\fille.jpg";

            //throw new NotImplementedException();
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public partial class MainPage : PhoneApplicationPage
    {
        protected ObservableCollection<Personne> lesPersonnes;
        protected Personne currentPersonne ;

        public ObservableCollection<Personne> LesPersonnes
        {
            get { return lesPersonnes; }
        }

        // Constructeur
        public MainPage()
        {
            InitializeComponent();

            lesPersonnes = new ObservableCollection<Personne>
            {
                new Personne("BILEM","Francois"),
                new Personne { Prenom="Hadrien", Nom="DELPHIN"},
                new Personne { Prenom="Joseph", Nom="IACOPETTA"}
            };

            lesPersonnes.Add( new Personne("Patrick", "BOGUET"));

            for (int i = 0; i < 50; i++)
            {
                lesPersonnes.Add(new Personne("KLEIN", "Nicolas"));
                lesPersonnes.Add(new Personne("KLEIN", "Sophie", false));
            }

            DataContext = this;
           

           



            // Exemple de code pour la localisation d'ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void MaListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currentPersonne = (Personne)MaListBox.SelectedItem;
            if (currentPersonne != null)
            {
                NavigationService.Navigate(new Uri("/FormPersonne.xaml", UriKind.Relative));
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            PhoneApplicationPage p = (PhoneApplicationPage)e.Content;
            p.DataContext = currentPersonne;
            base.OnNavigatedFrom(e);
        }

        private void Button_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            currentPersonne = new Personne() ;
            lesPersonnes.Add( currentPersonne ) ;
            NavigationService.Navigate(new Uri("/FormPersonne.xaml", UriKind.Relative));
        }

        // Exemple de code pour la conception d'une ApplicationBar localisée
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Définit l'ApplicationBar de la page sur une nouvelle instance d'ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Crée un bouton et définit la valeur du texte sur la chaîne localisée issue d'AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Crée un nouvel élément de menu avec la chaîne localisée d'AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}