﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel ;
using System.Runtime.CompilerServices;

namespace Tp2
{
    public class Personne : INotifyPropertyChanged
    {
        protected String    nom;
        protected String    prenom;
        protected int       age;
        protected bool      masculin;

        public Personne( String nom, String prenom, bool masculin=true, int age=0)
        {
            Nom = nom;
            Prenom = prenom;
            Age = age;
            Masculin = masculin;
        }

        public Personne()
        {
            masculin = true;
        }

        public String Nom
        {
            get { return nom; }
            set
            {
                ChangeProperty( ref nom, value);
            }
        }

        public String Prenom
        {
            get { return prenom; }
            set 
            {
                ChangeProperty(ref prenom, value);
            }
        }

        public int Age
        {
            get { return age; }
            set
            {
                ChangeProperty(ref age, value);
            }
        }


        public bool Masculin
        {
            get { return masculin; }
            set
            {
                ChangeProperty(ref masculin, value);
            }
        }

        public override string ToString()
        {
            return nom + " " + prenom;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged( [CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void ChangeProperty<T>( ref T attribute, T value, [CallerMemberName] String propertyName = "")
        {
            if( !object.Equals( attribute,value) )
            {
                attribute = value;
                NotifyPropertyChanged(propertyName);
            }
        }
    }
}
